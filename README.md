# Java注册登录及邮箱发送账号激活（主要技术栈SpringBoot，MyBatis）

## 前言
**项目流程图如下：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/ebcdb75c710540a19d8839d47972fcac.png)
这里我们通过：
> 1. 163邮箱来实现激活码发送
> 2. qq邮箱来进行接收

## 学习之前需要掌握的知识
1. springboot的基本使用方法
2. mysql的使用
3. mybatis的简单使用



## 项目环境搭建
这里我们直接使用 Spring Initializr 初始化 Spring Boot 项目
![在这里插入图片描述](https://img-blog.csdnimg.cn/0f36c1f81e4c4efc984b208a1e83f38e.png)
环境依赖选择：
1. lombok简化开发，使用注解，避免写重复性代码
2. SpringWeb，实现一个前后端的数据交互（一个登录注册验证，没必要写前后端分离）
3. MySQL 
4. MyBatis
![在这里插入图片描述](https://img-blog.csdnimg.cn/c6ad8b5a5b804cafb26d7490a28a8b9f.png)