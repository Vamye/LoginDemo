package com.expamle.emailloginregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailLoginRegisterApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailLoginRegisterApplication.class, args);
    }

}
