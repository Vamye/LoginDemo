package com.expamle.emailloginregister.controller;

import com.expamle.emailloginregister.pojo.User;
import com.expamle.emailloginregister.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Controller 不可使用RestControoler
 *
 * */
@Controller
public class SystemController {
    @Autowired
    private UserService userService;

    /**
     * 登录
     * */
    @GetMapping("/login")
    public String login(){

       return "login";
    }

    /**
     * 注册
     * */
    @GetMapping("/registry")
    public String registry(User user){



        return "registry";
    }

}
