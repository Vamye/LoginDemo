package com.expamle.emailloginregister.controller;

import com.expamle.emailloginregister.pojo.User;
import com.expamle.emailloginregister.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("users")
public class UserController {
@Autowired
    private UserService userService;

    /**
     * 注册账号
     * */
    @PostMapping("register")
    public Map<String,Object> registery(User user){

      Map<String,Object> map  =  userService.createAccount(user);

        return map;
    }

    /**
     * 用户登录
     * */
    @PostMapping("login")
    public Map<String,Object> login(User user){

        Map<String,Object> map = userService.accountLogin(user);

        return map;
    }

    /**
     * 账号激活
     * 激活码附在请求后
     *
     * */
    @GetMapping("activation")
    public Map<String,Object> activationAccount(String confirmCode){
        Map<String,Object> map = userService.activationAccount(confirmCode);

        return map;
    }


}
