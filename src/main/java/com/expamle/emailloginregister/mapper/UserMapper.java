package com.expamle.emailloginregister.mapper;

import com.expamle.emailloginregister.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 注册插入用户信息
     * */
    @Insert("insert into user(email,password,activation_time,confirm_code,is_vaild,salt,gmt_create,gmt_modified)" +
            "values(#{email},#{password},#{activationTime},#{confirmCode},#{isVaild},#{salt},#{gmtCreate},#{gmtModified})")
    int insertUser(User user);

    /**
     * 根据confirm_code查询与用户是否已激活
     * confirm_code
     * is_vaild
     * */
    @Select("select email,activation_time from user where confirm_code = #{confirmCode} and is_vaild = 0")
    User findUserByConfirmCode(@Param("confirmCode") String confirmCode);

    /**
     * 根据激活码查询用户 并 修改状态值
     * */
    @Update("update user set is_vaild=1 where confirm_code = #{confirmCode}")
    int confirmUser(@Param("confirmCode") String confirmCode);

    /**
     * 根据邮箱查询账号
     * */
    @Select("select email,password,salt from user where email=#{email} and is_vaild=1")
    List<User> findUserByEmail(@Param("email") String email);

    /**
     * 用户查重
     * */
    @Select("select count(*) from user where email=#{email} limit 1")
    int emailExist(@Param("email") String email);
}
