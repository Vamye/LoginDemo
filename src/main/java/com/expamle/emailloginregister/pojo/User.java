package com.expamle.emailloginregister.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

        private Integer id; // 主键Id

        private String email; // 邮箱

        private String password; // 密码 md5+盐

       /* @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")*/
        private LocalDateTime activationTime; // 激活失效时间

        private String confirmCode; // 确认代码

        private Integer isVaild; // 是否可用 0-不可用 1-可用

        private String salt; // 加密盐

/*
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
*/
        private LocalDateTime gmtCreate; // 创建时间

/*
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
*/
        private LocalDateTime gmtModified; // 修改时间
}
