package com.expamle.emailloginregister.service;

public interface EmailService {

    void sendEmail(String email,String activationUrl);

}
