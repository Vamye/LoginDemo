package com.expamle.emailloginregister.service.Impl;

import com.expamle.emailloginregister.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 * 发送邮件相关业务
 * */
@Service
public class EmailServiceImpl implements EmailService {

    // 引入配置文件属性
    @Value("${spring.mail.username}")
    private String sendUsername;


    // javamain mail依赖方法---此方法无法使用autowird注入，javaMailSender不是springboot的方法
    @Resource
    private JavaMailSender javaMailSender;

    @Resource //同上
    private TemplateEngine templateEngine;

    /**
     * 发送方法
     * */

    public void sendEmail(String email,String activationUrl){

        // 创建邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {

            /**
             * MineMessagehelper---设置邮件相关内容
             * @Params1 邮件对象
             * @Params2 是否发送多封邮件
             * */
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);

            // 设置邮件主题
            message.setSubject("注册账号激活");

            // 设置邮件发送者
            message.setFrom(sendUsername);

            // 设置邮件接收者，可多个
            message.setTo(email);

            // 设置邮件抄送人
           /* message.setCc();*/

            // 设置邮件隐秘抄送人,可多个
            /*message.setBcc();*/

            // 设置邮件发送日期
            message.setSentDate(new Date());

            // 创建上下文环境--thym依赖提供方法，使用当前本地前端
            Context context = new Context();
            // 邮件中传递的链接
            context.setVariable("activationUrl",activationUrl);


            // 映射html文件
            String text = templateEngine.process("activation-account.html",context);

            // 设置邮件正文-true-是否是html模板
            message.setText(text,true);

                   } catch (MessagingException e) {
            e.printStackTrace();
        }

        // 发送邮件
        javaMailSender.send(mimeMessage);

    }

}
