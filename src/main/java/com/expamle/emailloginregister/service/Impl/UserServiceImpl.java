package com.expamle.emailloginregister.service.Impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import com.expamle.emailloginregister.mapper.UserMapper;
import com.expamle.emailloginregister.pojo.User;
import com.expamle.emailloginregister.service.EmailService;
import com.expamle.emailloginregister.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
   @Autowired
   private UserMapper userMapper;

   @Autowired
   private EmailService emailService;

   /**
    * 用户注册
    * */
    @Override
    public Map<String,Object> createAccount(User user) {

        // 统一返回对象
        Map<String,Object> result = new HashMap<>();

        /**
         * 注册--构建User对象内的数据
         *  前端返回参数 email、password
         * */

        // 判断邮箱是否已经存在
        int emailExist = userMapper.emailExist(user.getEmail());
        if (emailExist == 1){
            result.put("code",400);
            result.put("msg","用户已存在");
            return result;
        }

        //开始构建user数据

        // 激活码--hutool工具生成雪花算法
        String confirmCode = IdUtil.getSnowflake(1,1).nextIdStr();
        user.setConfirmCode(confirmCode);

        // 加密密码  password+盐+md5
        String password = user.getPassword();
        String salt = RandomUtil.randomString(6); // hutool随机生成6个字符
        String md5Pwd = SecureUtil.md5(password+salt);

        // 设置user参数、salt、password
        user.setSalt(salt);
        user.setPassword(md5Pwd);

        // 是否已激活 默认-0 未激活
        user.setIsVaild(0);

        // 激活码有效期
        LocalDateTime activationTime = LocalDateTime.now().plusDays(1); // 当前时间增加1天时间
        user.setActivationTime(activationTime);

        // 创建时间
        LocalDateTime nowTime = LocalDateTime.now();
        user.setGmtCreate(nowTime);
        user.setGmtModified(nowTime);

        // 用户信息插入数据库
        int insertUser = userMapper.insertUser(user);

        // 判断数据库写入是否成功并返回相关信息
        if (insertUser > 0){

            // 准备激活链接
            String activationUrl ="http://localhost:8001/users/activation?confirmCode="+confirmCode;

            //发送邮箱信息
             emailService.sendEmail(user.getEmail(), activationUrl);

            result.put("code",200);
            result.put("msg","注册成功，请前往邮箱激活账号");
            return result;
        }else{
            result.put("code",400);
            result.put("msg","注册失败");
            return result;
        }
    }

    public static void main(String[] args) {
        String a = SecureUtil.md5("123456"+"qldp40");
        System.out.println(a);
    }

    /**
     * 用户登录
     * */
    @Override
    public Map<String, Object> accountLogin(User user) {
        // 统一返回数据
        Map<String,Object> result = new HashMap<>();


        // 前端数据校验
       if (user.getEmail() == null || user.getEmail().isEmpty() ||
            user.getPassword() == null || user.getPassword().isEmpty()
           ){
                result.put("code",400);
                result.put("msg","请输入账号和密码");
                return result;
       }


       // 查询用户是否在数据库中
        List<User> users = userMapper.findUserByEmail(user.getEmail());

        // 判断用户是否异常
        if (users == null || users.isEmpty()){
            result.put("code",400);
            result.put("msg","账号未注册或未激活");
            return result;
        }

        // 查询出多个账号
        if (users.size()>1){
            result.put("code",400);
            result.put("msg","账号异常请联系管理员");
            return result;
        }

        // 得到唯一用户-判断账号是否激活
        User DbUser = users.get(0);

        // 校验密码-获取加密数据
        String salt = DbUser.getSalt();
        String md5Pwd = SecureUtil.md5(user.getPassword()+salt);

        /**
         * DbUser.getPassword().equals(md5Pwd)---可以匹配
         * md5Pwd.equals(DbUser.getPassword())---无法匹配 ??????
         * */
        if (DbUser.getPassword().equals(md5Pwd)){
            result.put("code",200);
            result.put("msg","登录成功");
            return result;
        }else {
            result.put("code",400);
            result.put("msg","账号或密码有误");
            return result;
        }

    }

    /**
     * 激活账号
     * */
    @Override
    public Map<String, Object> activationAccount(String confirmCode) {
        // 统一返回参数
        Map<String, Object> map = new HashMap<>();

        // 通过confirmCount查询用户
        User user = userMapper.findUserByConfirmCode(confirmCode);

        System.out.println(user.toString());

        if (user == null ){
            map.put("code",400);
            map.put("msg","账号未注册");
            return map;
        }

        // 判断激活码有效期
        // 当前时间是否在激活码截止期之后
        boolean after = LocalDateTime.now().isAfter(user.getActivationTime());
        if (after){
            map.put("code",400);
            map.put("msg","链接已失效，请重新获取激活码");
            return map;
        }

        // 激活账号，修改状态为 1
        int confirmUser = userMapper.confirmUser(confirmCode);
        if (confirmUser == 1){
            map.put("code",200);
            map.put("msg","激活账号成功");
            return map;
        }else{
            map.put("code",400);
            map.put("msg","激活账号失败");
            return map;
        }

    }
}
