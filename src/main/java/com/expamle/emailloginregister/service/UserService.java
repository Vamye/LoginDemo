package com.expamle.emailloginregister.service;

import com.expamle.emailloginregister.pojo.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
public interface UserService {

    Map<String,Object> createAccount(User user);

    Map<String, Object> accountLogin(User user);

    Map<String, Object> activationAccount(String confirmCode);
}
